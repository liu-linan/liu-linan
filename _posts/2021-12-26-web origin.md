---
title: "如何fork别人的仓库并导入gitee"
layout: page
excerpt_separator: "<!--more-->"
categories:
  - 网站设计

---
## 想在别人仓库的基础上进行更改成我喜欢的页面形式怎么办 ##
###  但我不会使用github修改仓库内容！？ ###
<!--more-->
* 我们在GitHub上fork别人的仓库不会改，是可以通过gitee去改的，
下面我演示一下过程

我们需要找到自己想要fork的仓库,然后看到页面的右上角fork，点击它
![](/assets/images/web1.png)

这个页面就是显示我们成功fork到别人的仓库了
![](/assets/images/web2.png)

接下来，我们要打开gitee，把鼠标移动到“+”图标，就会出现下面图片上的信息，我们点击“从github导入仓库”
![](/assets/images/web3.png)
![](/assets/images/web4.png)

最后我们需要再次打开GitHub找到页面中的“code”，复制它
![](/assets/images/web5.png)
复制完之后回到导入GitHub的页面，黏贴url选择前面fork的仓库就可以啦！
## 最最后，你就可以在原作者仓库的基础上进行更改成自己喜欢的页面了 ##